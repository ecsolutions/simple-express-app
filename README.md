![ExpressJS Logo][express-logo]

[express-logo]: https://expressjs.com/images/express-facebook-share.png "ExpressJS Website"

# Sample Express Application

This repository's only purpose is to give you a basic idea of how you could go about
structuring your backend application. As mentioned during lecture, there is no one
design pattern that is objectively the best approach. Here we apply a very common pattern,
MVC (Model-View-Controller). There are tons and tons of information about different approaches
to structuring projects and I do not recommend that you dvelve too far into these concepts
unless, of course, you find it interesting.

I can, however, recommend that you watch [this video][express and mvc] available on YouTube
where the MVC design pattern is explained quite clearly in an ExpressJS context.

[express and mvc]: https://www.youtube.com/watch?v=zW_tZR0Ir3Q
