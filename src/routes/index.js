const apiRouter = require("./api");
const viewsRouter = require("./views");

module.exports = [
    { path: "/api", router: apiRouter },
    viewsRouter,
];
