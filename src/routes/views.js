const router = require("express").Router();

router.get("/", (_request, response) => {
    response.sendFile("index.html");
});

module.exports = router;