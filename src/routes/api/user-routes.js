const userRouter = require("express").Router();
const userControllers = require("../../controllers/user-controller");

userRouter.get("/users", userControllers.getUsers);
userRouter.get("/users/:id", userControllers.getUser);
userRouter.post("/users", userControllers.createUser);
userRouter.delete("/users/:id", userControllers.deleteUser);

module.exports = userRouter;
