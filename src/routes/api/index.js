/**
 * Files in the routes-directory are only meant as a way to couple
 * different routes with their respective route controller / handler
 */

const router = require("express").Router();
const userRouter = require("./user-routes");

router.use(userRouter);

module.exports = router;