const config = require("./config");
const express = require("express");
const routers = require("./routes");

let server = express();

function loadMiddlewares() {
    server.use(express.json());
    server.use(express.static("public"));
}

function loadRouters() {
    routers.forEach(router => {
        if (router.path) {
            server.use(router.path, router.router);
        } else {
            server.use(router);
        }
    });
}

function listen() {
    server.listen(config.PORT, () => {
        console.info(`Server is running on port ${config.PORT}.`);
    });
}

function start() {
    loadMiddlewares();
    loadRouters();
    listen();
}

module.exports = {
    start,
};
