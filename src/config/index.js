require("dotenv").config();

module.exports = {
    PORT: process.env.PORT,
    HOSTNAME: process.env.HOSTNAME,
};
