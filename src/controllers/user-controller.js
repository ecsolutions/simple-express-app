/**
 * Controllers handle business logic (if you have any) and acts
 * as your request's "brain", delegating more specific tasks
 * to other parts of your backend.
 */

const User = require("../models/user-model");

exports.getUsers = (_request, response) => {
    response.send(User.getAll());
};

exports.getUser = (request, response) => {
    response.send(User.getById(request.params.id));
};

exports.createUser = (request, response) => {
    const user = User.create(request.body);

    if (user) {
        response.send(user);
    } else {
        response.sendStatus(422);
    }
};

exports.deleteUser = (request, response) => {
    const user = User.deleteById(request.params.id);

    if (user) {
        response.send(user);
    } else {
        response.sendStatus(204);
    }
};