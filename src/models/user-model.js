/**
 * By models we are creating an abstraction layer for different
 * database entities (tables, collections, etc.)
 */

const { users } = require("../database");

module.exports = class User {

    /**
     * Returns all users from the database
     * 
     * @returns User[]
     */
    static getAll() {
        return users;
    };
    
    /**
     * Returns a specific user from the database
     * 
     * @param {number} id 
     * @returns null | User
     */
    static getById(id) {
        return users.find((user) => user.id === parseInt(id));
    };

    /**
     * Adds a new user to the "database" and returns it with modifications
     * 
     * @param {object} userData 
     * @returns null | User
     */
    static create(userData) {
        if (!userData) return null;
        
        const databaseIsEmpty = users.length === 0;
        
        let newUserId;

        if (databaseIsEmpty) {
            newUserId = 1;
        } else {
            newUserId = users[users.length - 1].id + 1;
        }

        const userDataWithId = {
            id: newUserId,
            ...userData,
        };

        users.push(userDataWithId);

        return userDataWithId;
    }
    
    /**
     * Returns false if no user can be found,
     * removes user from database and returns it otherwise.
     * 
     * @param {number} id 
     * @returns false | User;
     */
    static deleteById(id) {
        const userIndex = users.findIndex((user) => user.id === parseInt(id))

        if (userIndex === -1) {
            return false;
        }

        const user = users.splice(userIndex, 1);
    
        return user;
    };

}