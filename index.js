/**
 * An application entry point, usually `app.js` or `index.js` is a perfect place
 * to do more environmental configurations before actually starting your server.
 * 
 * Although this might not be applicable to your project, it might be good to know
 * that this is a quite common practice.
 */

const Server = require("./src/server");

Server.start();